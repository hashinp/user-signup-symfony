<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'attr'=> [
                    'placeholder' => 'First Name',
                ],
                'label' => false,
            ])
            ->add('lastName', TextType::class, [
                'attr'=> [
                    'placeholder' => 'Last Name',
                ],
                'label' => false,
            ])
            ->add('email', EmailType::class, [
                'attr'=> [
                    'placeholder' => 'Email',
                ],
                'label' => false,
            ])
            ->add('password', PasswordType::class, [
                'attr'=> [
                    'placeholder' => 'Password',
                ],
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

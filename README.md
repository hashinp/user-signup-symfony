#Sample symfony application for User signup

After cloning the repo, start the application by executing

    bin/console server:start
    
Access the application at the url provided in the output of the above command.

## Access sign up page

http:://<host>:<port>/signup

## See signed up users
http:://<host>:<port>/user

No authorization is implemented